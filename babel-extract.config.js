module.exports = {
  presets: ['babel-preset-gatsby'],
  plugins: [
    [
      'i18next-extract',
      {
        locales: ['de', 'en', 'fr'],
        keySeparator: null,
        nsSeparator: null,
        keyAsDefaultValue: ['de'],
        useI18nextDefaultValue: ['de'],
        discardOldKeys: true,
        defaultNS: 'de',
        outputPath: 'locales/{{locale}}/{{ns}}.json',
        customTransComponents: [
          ['gatsby-plugin-react-i18next', 'Trans'],
        ],
        customUseTranslationHooks: [
          ['gatsby-plugin-react-i18next', 'useTranslation']
        ]
      }
    ]
  ],
  overrides: [
    {
      test: [`**/*.ts`, `**/*.tsx`],
      plugins: [[`@babel/plugin-transform-typescript`, { isTSX: true }]]
    }
  ]
};