import * as React from "react";
import TopNav from "./topnav";
import Footer from "./footer";
// import ResponsiveDebug from "./responsive-debug";

const Layout = ({ location, children }) => {
  return (
    <>
      {/* <ResponsiveDebug /> */}
      <TopNav />
      <main>{children}</main>
      <Footer />
    </>
  );
};

export default Layout;
